package com.tw.cicd.cicid;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc


public class ApplicationControllerIntegrationTests

{

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void index_should_return_application_info() throws Exception {

        this.mockMvc.perform(get("/application")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Ankur and Mohit's store"));

    }

    @Test
    public void index_should_fail_for_wrong_input_info() throws  Exception{
        this.mockMvc.perform(get("/applictaion")).andDo(print()).andExpect(status().is4xxClientError());
    }
}
