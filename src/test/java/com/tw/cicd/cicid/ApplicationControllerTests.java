package com.tw.cicd.cicid;

        import com.tw.cicd.cicid.controllers.ApplicationController;
        import org.junit.Test;

        import static org.junit.Assert.assertNotNull;
        import static org.junit.Assert.assertTrue;

public class ApplicationControllerTests
{

    @Test

    public void should_return_application_info(){

        ApplicationController controller = new ApplicationController();
        ApplicationInfo appInfo = controller.index() ;
        assertNotNull(appInfo);

    }


}
