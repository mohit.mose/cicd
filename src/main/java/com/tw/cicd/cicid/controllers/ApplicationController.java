package com.tw.cicd.cicid.controllers;
import com.tw.cicd.cicid.ApplicationInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.stereotype.Controller;

@RestController
public class ApplicationController {

    @GetMapping("/application")
    public ApplicationInfo index() {
        return new ApplicationInfo();
    }
}
